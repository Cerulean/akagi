//
//  CPBaseViewController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPCommonTools.h"

@class CPBaseViewModel;

@interface CPBaseViewController : UIViewController

@property (nonatomic, strong) CPBaseViewModel *viewModel;

@end
