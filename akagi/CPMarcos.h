//
//  CPMarcos.h
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#ifndef CPMarcos_h
#define CPMarcos_h

#define WeakSelf   __weak typeof(self) w_self = self
#define StrongSelf __strong typeof(w_self) s_self = w_self

#define SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height

#define STATUS_HEIGHT 20
#define TABBAR_HEIGHT 50
#define COMMON_HEIGHT 44

#define CPImageWithName(__IMAGE_NAME__) [UIImage imageNamed:__IMAGE_NAME__]

#define kNeedLoginNotification @"kNeedLoginNotification"

#endif /* CPMarcos_h */
