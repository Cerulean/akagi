//
//  CPModifyInfoController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPModifyInfoController.h"
#import "CPPickerView.h"
#import "CPSchoolCell.h"
#import "CPCheckCell.h"

#import "CPCheckViewController.h"
#import "CPSearchViewController.h"
#import "CPCardPickerController.h"

@implementation CPModifyInfoController {
    UITableView *_tableView;
    
    NSArray *_firstCells;
    NSArray *_secondCells;
    NSArray *_thirdCells;
    NSArray *_fourthCells;
    
    NSArray *_cellCollections;
    
    CPPickerView *_pickerView;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"完善信息";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *rightItem = [CPBarButtonItem itemWithTitle:@"完成"
                                                          Color:ColorMainTextContent
                                                         Target:self
                                                         Action:@selector(finishModify:)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    [self setupTableView];
    [self setupCells];
    
    _pickerView = [[CPPickerView alloc] initWithObjects:nil containerController:self];
    
    _manager = [CPManager new];
    _manager.sex = CPManagerGenderMale;
}

- (void)setupTableView {
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.delegate        = self;
    _tableView.dataSource      = self;
    _tableView.contentInset    = UIEdgeInsetsMake(0, 0, 200, 0);
    _tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = ColorSection;
    
    [self.view addSubview:_tableView];
}

- (void)setupCells {
    WeakSelf;
    
    _nameLabel = [CPTextFieldCell new];
    _nameLabel.title = @"姓名";
    _nameLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的姓名"
                                                                                          Color:ColorPlacehodler
                                                                                       FontSize:17];
    _nameLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.name = text;
    };
    
    _sexLabel = [CPTextLabelCell new];
    _sexLabel.title   = @"性别";
    _sexLabel.content = @"男";
    _sexLabel.contentView.tapAction = ^(UIView *cell) {
        
        StrongSelf;
        
        [s_self hideKeyboardForCells];
        
        [s_self->_pickerView setPickerObjects:@[@[@"男", @"女"]]];
        [s_self->_pickerView show];
        [s_self->_pickerView setBlock:^(NSArray *pickedObjects) {
            
            __strong typeof(w_self) ss_self = w_self;
            
            ss_self.sexLabel.content = [pickedObjects firstObject];
            ss_self.manager.sex = [ss_self.sexLabel.content isEqualToString:@"男"] ? CPManagerGenderMale : CPManagerGenderFemale;
        }];
    };
    
    _emailLabel = [CPTextFieldCell new];
    _emailLabel.title = @"邮箱";
    _emailLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的邮箱"
                                                                                          Color:ColorPlacehodler
                                                                                       FontSize:17];
    _emailLabel.textField.keyboardType = UIKeyboardTypeEmailAddress;
    _emailLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.email = text;
    };
    
    _qqLabel = [CPTextFieldCell new];
    _qqLabel.title = @"QQ";
    _qqLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的QQ"
                                                                                        Color:ColorPlacehodler
                                                                                     FontSize:17];
    _qqLabel.textField.keyboardType = UIKeyboardTypeNumberPad;
    _qqLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.qq = text;
    };
    
    _idCardLabel = [CPTextFieldCell new];
    _idCardLabel.title = @"身份证";
    _idCardLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的身份证号码"
                                                                                            Color:ColorPlacehodler
                                                                                         FontSize:17];
    _idCardLabel.textField.keyboardType = UIKeyboardTypeNumberPad;
    _idCardLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.idCardNumber = text;
    };
    
    _alipayLabel = [CPTextFieldCell new];
    _alipayLabel.title = @"支付宝";
    _alipayLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的支付宝"
                                                                                            Color:ColorPlacehodler
                                                                                         FontSize:17];
    _alipayLabel.textField.keyboardType = UIKeyboardTypeEmailAddress;
    _alipayLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.alipayAccount = text;
    };
    
    _graduateLabel = [CPTextFieldCell new];
    _graduateLabel.title = @"毕业年份";
    _graduateLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的毕业年份"
                                                                                              Color:ColorPlacehodler
                                                                                           FontSize:17];
    _graduateLabel.textField.keyboardType = UIKeyboardTypeNumberPad;
    _graduateLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.graduateYear = text;
    };
    
    _typeLabel = [CPTextLabelCell new];
    _typeLabel.title = @"身份";
    _typeLabel.contentView.tapAction = ^(UIView *cell) {
        
        StrongSelf;
        
        [s_self hideKeyboardForCells];
        
        [s_self->_pickerView setPickerObjects:@[@[@"配送", @"打印"]]];
        [s_self->_pickerView show];
        
        [s_self->_pickerView setBlock:^(NSArray *pickedObjects) {
            
            __strong typeof(w_self) ss_self = w_self;
            
            ss_self.typeLabel.content = [pickedObjects firstObject];
            
            ss_self.manager.type = [ss_self.typeLabel.content isEqualToString:@"配送"] ? CPManagerTypeDeliver : CPManagerTypePrinter;
            
            ss_self.manageBuildingsLabel.userInteractionEnabled = ss_self.manager.type == CPManagerTypeDeliver ? NO : YES;
            
            ss_self.manager.building        = nil;
            ss_self.manager.manageBuildings = nil;
            
            ss_self.buildingLabel.content        = nil;
            ss_self.manageBuildingsLabel.content = nil;
        }];
    };
    
    _schoolLabel = [CPTextLabelCell new];
    _schoolLabel.title = @"学校";
    _schoolLabel.contentView.tapAction = ^(UIView *cell) {
        
        StrongSelf;
        
        [s_self hideKeyboardForCells];
        
        CPBaseViewModel *viewModel = [[CPBaseViewModel alloc] initWithAPI:@"address/school" CellClass:[CPSchoolCell class]];
        
        CPSearchViewController *schoolChooseController = [CPSearchViewController new];
        schoolChooseController.viewModel = viewModel;
        schoolChooseController.title = @"选择学校";
        schoolChooseController.enableUpRefresh    = NO;
        schoolChooseController.enableDownRrefresh = NO;
        
        __weak CPSearchViewController *w_schoolController = schoolChooseController;
        
        schoolChooseController.action = ^(CPBaseModel *model) {
            s_self.manager.school = (CPSchool *)model;
            s_self.schoolLabel.content = s_self.manager.school.name;
            
            s_self.manager.building = nil;
            s_self.buildingLabel.content = nil;
            
            [w_schoolController.navigationController popViewControllerAnimated:YES];
        };
        
        [s_self.navigationController pushViewController:schoolChooseController animated:YES];
    };
    
    _buildingLabel = [CPTextLabelCell new];
    _buildingLabel.title = @"所在宿舍";
    _buildingLabel.contentView.tapAction = ^(UIView *cell) {
        
        StrongSelf;
        
        [s_self hideKeyboardForCells];
        
        NSMutableArray *buildingNames = [NSMutableArray new];
        
        for (CPBuilding *building in s_self.manager.school.buildings) {
            [buildingNames addObject:building.name];
        }
        
        [s_self->_pickerView setPickerObjects:@[buildingNames]];
        [s_self->_pickerView show];
        
        [s_self->_pickerView setBlock:^(NSArray *pickedObjects) {
            
            __strong typeof(w_self) ss_self = w_self;
            
            NSString *buildingName = [pickedObjects firstObject];

            for (CPBuilding *building in ss_self.manager.school.buildings) {
                if ([buildingName isEqualToString:building.name]) {
                    ss_self.manager.building = building;
                    ss_self.buildingLabel.content = ss_self.manager.building.name;
                    
                    if ([ss_self.typeLabel.content isEqualToString:@"配送"]) {
                        ss_self.manageBuildingsLabel.content = @"已选择";
                        ss_self.manager.manageBuildings = @[ss_self.manager.building];
                    }
                    
                    break;
                }
            }
        }];
    };
    
    _manageBuildingsLabel = [CPTextLabelCell new];
    _manageBuildingsLabel.title = @"管辖宿舍";
    _manageBuildingsLabel.content = @"未选择";
    _manageBuildingsLabel.contentView.tapAction = ^(UIView *cell) {
        
        StrongSelf;
        
        CPBaseViewModel *viewModel = [[CPBaseViewModel alloc] initWithAPI:nil CellClass:[CPCheckCell class]];
        
        NSMutableArray *buildings = [s_self.manager.school.buildings mutableCopy];
        [buildings removeObject:s_self.manager.building];
        
        viewModel.objects = buildings;
        
        CPCheckViewController *checkViewController = [CPCheckViewController new];
        checkViewController.viewModel = viewModel;
        checkViewController.enableUpRefresh    = NO;
        checkViewController.enableDownRrefresh = NO;
        checkViewController.block = ^(NSArray *results) {
            
            __strong typeof(w_self) ss_self = w_self;
            
            // CheckViewController 确保 results 不为空
            
            NSMutableArray *buildings = [results mutableCopy];
            [buildings addObject:s_self.manager.building];
            
            ss_self.manager.manageBuildings = [buildings copy];
            ss_self.manageBuildingsLabel.content = @"已选择";
        };
        
        [s_self.navigationController pushViewController:checkViewController animated:YES];
    };
    
    _dormLabel = [CPTextFieldCell new];
    _dormLabel.title = @"宿舍";
    _dormLabel.textField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"您的宿舍"
                                                                                          Color:ColorPlacehodler
                                                                                       FontSize:17];
    _dormLabel.textField.textAction = ^(NSString *text) {
        StrongSelf;
        s_self.manager.dorm = text;
    };
    
    _cardLabel = [CPTextLabelCell new];
    _cardLabel.title = @"上传证件";
    _cardLabel.content = @"未上传";
    _cardLabel.contentView.tapAction = ^(UIView *cell) {
        
        StrongSelf;
        
        [s_self hideKeyboardForCells];
        
        CPCardPickerController *cardPickerController = [[CPCardPickerController alloc] init];
        cardPickerController.title = @"上传证件";
        cardPickerController.block = ^(UIImage *firstImage) {
            
            // cardPickerController 确保firstImage与secondImage同时不为空
            
            NSData *imageData = UIImageJPEGRepresentation(firstImage, 0.01);
            
            s_self.manager.studentCard = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
            
            s_self.cardLabel.content = @"已上传";
        };
        
        [s_self.navigationController pushViewController:cardPickerController animated:YES];
    };
    
    _firstCells  = @[_nameLabel, _sexLabel, _qqLabel, _emailLabel];
    _secondCells = @[_alipayLabel, _idCardLabel];
    _thirdCells  = @[_typeLabel, _graduateLabel, _schoolLabel, _buildingLabel, _manageBuildingsLabel, _dormLabel];
    _fourthCells = @[_cardLabel];

    _cellCollections = @[_firstCells, _secondCells, _thirdCells, _fourthCells];
    
    _emailLabel.line.hidden  = YES;
    _idCardLabel.line.hidden = YES;
    _dormLabel.line.hidden   = YES;
    _cardLabel.line.hidden   = YES;
}

- (void)hideKeyboardForCells {
    for (NSArray *cells in _cellCollections) {
        for (CPBaseCell *cell in cells) {
            if ([cell isKindOfClass:[CPTextFieldCell class]]) {
                [[(CPTextFieldCell *)cell textField] resignFirstResponder];
            }
        }
    }
}

- (void)finishModify:(UIBarButtonItem *)sender {
    
    if (![CPValidater validateName:_manager.name]) {
        [CPAlert alertWithTitle:@"错误" Message:@"中文姓名长度不合法"];
        return;
    }
    
    if (![CPValidater validateEmail:_manager.email]) {
        [CPAlert alertWithTitle:@"错误" Message:@"邮箱格式不合法"];
        return;
    }
    
    if (![CPValidater validateQQ:_manager.qq]) {
        [CPAlert alertWithTitle:@"错误" Message:@"QQ号码长度不合法"];
        return;
    }
    
    if (![CPValidater validateIdentityCard:_manager.idCardNumber]) {
        [CPAlert alertWithTitle:@"错误" Message:@"身份证长度不合法"];
        return;
    }
    
    if (![CPValidater validateEmail:_manager.alipayAccount] && ![CPValidater validateMobile:_manager.alipayAccount]) {
        [CPAlert alertWithTitle:@"错误" Message:@"支付宝账号格式不合法"];
        return;
    }
    
    if (![CPValidater validateGraduateYear:_manager.graduateYear]) {
        [CPAlert alertWithTitle:@"错误" Message:@"毕业年份不合法"];
        return;
    }
    
    if (_manager.type == CPManagerTypeUnknown) {
        [CPAlert alertWithTitle:@"错误" Message:@"身份类型不能为空"];
        return;
    }
    
    if (!_manager.school) {
        [CPAlert alertWithTitle:@"错误" Message:@"学校不能为空"];
        return;
    }
    
    if (!_manager.building) {
        [CPAlert alertWithTitle:@"错误" Message:@"所在宿舍楼不能为空"];
        return;
    }
    
    if (!_manager.manageBuildings) {
        [CPAlert alertWithTitle:@"错误" Message:@"未选择配送宿舍楼"];
        return;
    }
    
    if (!_manager.studentCard) {
        [CPAlert alertWithTitle:@"错误" Message:@"证件未上传"];
        return;
    }
    
    NSDictionary *parms = nil;
    
    parms = [MTLJSONAdapter JSONDictionaryFromModel:_manager error:NULL];
    
    WeakSelf;
    
    [CPWebService commonRequestWithAPI:@"manager/info" Method:@"POST" Parms:[parms copy] Block:^(BOOL success, NSDictionary *resultDic) {
        if (success) {
            StrongSelf;
            
            [CPWebService setManagerState:CPManagerStateAuthorizing];
            
            [s_self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

#pragma mark - TableView DataSource & Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _cellCollections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cellCollections[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return _cellCollections[indexPath.section][indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 19;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;
}

@end
