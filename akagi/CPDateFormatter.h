//
//  CPDateFormatter.h
//  akagi
//
//  Created by 王澍宇 on 15/10/19.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CPDateFormatter : NSObject

+ (NSDate *)dateFromString:(NSString *)dateString;

+ (NSString *)stringFromDate:(NSDate *)date;

+ (NSString *)stringFromValue:(NSString *)value;

@end
