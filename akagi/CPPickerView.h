//
//  CPPickerView.h
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPCommonTools.h"

#import "CPMarcos.h"

@class CPButton;

typedef void(^CPPickerFinishBlock)(NSArray *pickedObjects);

@interface CPPickerView : UIView

@property (nonatomic, strong) UIPickerView *pickerView;

@property (nonatomic, strong) CPButton *cancelButton;

@property (nonatomic, strong) CPButton *finishButton;

@property (nonatomic, strong) NSArray *pickerObjects;

@property (nonatomic, strong) CPPickerFinishBlock block;

- (instancetype)initWithObjects:(NSArray *)array containerController:(CPBaseViewController *)controller;

- (void)show;

- (void)hide;

@end
