//
//  CPOrderCell.m
//  akagi
//
//  Created by 王澍宇 on 15/10/19.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderCell.h"

@implementation CPOrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _topMarginView  = [UIView new];
        _topMarginView.backgroundColor = ColorSection;
        
        _downMarginView = [UIView new];
        _downMarginView.backgroundColor = ColorSection;
        
        _typeIcon     = [[UIImageView alloc] initWithImage:CPImageWithName(@"Order_Print")];
        _locationIcon = [[UIImageView alloc] initWithImage:CPImageWithName(@"Order_Location")];
        
        _typeLabel     = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:12 Alignment:NSTextAlignmentLeft];
        _locationLabel = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:12 Alignment:NSTextAlignmentRight];
        
        _infoView = [[UIView alloc] init];
//        _infoView.backgroundColor = ColorSubBackground;
        
        _nickNameLabel = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentLeft];
        _telLabel      = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentLeft];
        _dormLabel     = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentLeft];
        
        _arrow = [[UIImageView alloc] initWithImage:CPImageWithName(@"Order_Arrow")];
        
        _paidIcon   = [[UIImageView alloc] initWithImage:CPImageWithName(@"Order_Paid")];
        _demandIcon = [[UIImageView alloc] initWithImage:CPImageWithName(@"Order_Need")];
        
        _paidDateLabel   = [UILabel labelWithText:nil Color:ColorSubTextContent FontSize:10 Alignment:NSTextAlignmentLeft];
        _demandDateLabel = [UILabel labelWithText:nil Color:ColorWarning        FontSize:10 Alignment:NSTextAlignmentLeft];
        
        _handleButton = [CPButton buttonWithType:UIButtonTypeSystem Title:nil FontSize:12 ActionBlock:nil];
        [_handleButton setTitleColor:ColorWarning forState:UIControlStateNormal];
        [_handleButton.layer setCornerRadius:4.0];
        [_handleButton.layer setBorderWidth:1.0];
        [_handleButton.layer setBorderColor:ColorWarning.CGColor];
        
        WeakSelf;
        
        [_handleButton setActionBlock:^(CPButton *button) {
            
            StrongSelf;
            
            CPOrder *order = (CPOrder *)s_self.model;
            
            if (![order isKindOfClass:[CPOrder class]]) {
                return;
            }
            
            NSDictionary *parms = @{@"order_ID" : @(order.ID)};
            
            [CPWebService commonRequestWithAPI:@"orders/confirm" Method:@"POST" Parms:[parms copy] Block:^(BOOL success, NSDictionary *resultDic) {
                if (success) {
                    __strong typeof(w_self) ss_self = w_self;
                    
                    NSIndexPath *indexPath = [ss_self.tableView indexPathForCell:ss_self];
                    
                    [ss_self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                }
            }];
        }];
        
        [self.contentView addSubview:_topMarginView];
        [self.contentView addSubview:_downMarginView];
        
        [self.contentView addSubview:_typeIcon];
        [self.contentView addSubview:_locationIcon];
        
        [self.contentView addSubview:_typeLabel];
        [self.contentView addSubview:_locationLabel];
        
        [self.contentView addSubview:_infoView];
        
        [_infoView addSubview:_nickNameLabel];
        [_infoView addSubview:_telLabel];
        [_infoView addSubview:_dormLabel];
        
        [_infoView addSubview:_arrow];
        
        [self.contentView addSubview:_paidIcon];
        [self.contentView addSubview:_demandIcon];
        
        [self.contentView addSubview:_paidDateLabel];
        [self.contentView addSubview:_demandDateLabel];
        [self.contentView addSubview:_handleButton];
        
        [_topMarginView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(@0);
            make.height.equalTo(@20);
        }];
        
        [_typeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_topMarginView.mas_bottom).offset(10);
            make.left.equalTo(@10);
        }];
        
        [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_typeIcon);
            make.left.equalTo(_typeIcon.mas_right).offset(5);
            make.width.lessThanOrEqualTo(@200);
        }];
        
        [_locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_typeLabel);
            make.right.equalTo(@(-15));
        }];
        
        [_locationIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_locationLabel);
            make.right.equalTo(_locationLabel.mas_left).offset(-5);
        }];
        
        [_infoView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(@0);
            make.top.equalTo(_typeLabel.mas_bottom).offset(10);
        }];
        
        [_nickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.equalTo(@10);
            make.right.equalTo(@(-10));
        }];
        
        [_telLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_nickNameLabel.mas_bottom).offset(10);
            make.left.equalTo(_nickNameLabel);
            make.right.equalTo(_nickNameLabel);
        }];
        
        [_dormLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_telLabel.mas_bottom).offset(10);
            make.left.equalTo(_nickNameLabel);
            make.right.equalTo(_nickNameLabel);
            make.bottom.equalTo(@(-10));
        }];
        
        [_arrow mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.right.equalTo(@(-15));
        }];
        
        [_paidIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@10);
            make.top.equalTo(_infoView.mas_bottom).offset(10);
        }];
        
        [_paidDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_paidIcon);
            make.left.equalTo(_paidIcon.mas_right).offset(5);
            make.width.lessThanOrEqualTo(@180);
        }];
        
        [_demandIcon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_paidIcon);
            make.top.equalTo(_paidDateLabel.mas_bottom).offset(10);
        }];
        
        [_demandDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_demandIcon.mas_right).offset(8);
            make.centerY.equalTo(_demandIcon);
            make.width.equalTo(_paidDateLabel);
        }];
        
        [_handleButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_paidDateLabel).offset(10);
            make.right.equalTo(@(-10));
            make.width.equalTo(@(80));
        }];
        
        [_downMarginView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_handleButton.mas_bottom).offset(10);
            make.left.right.bottom.equalTo(@0);
        }];
        
        self.line.hidden = YES;
    }
    
    return self;
}

- (void)bindWithModel:(CPBaseModel *)model {
    self.model = model;
    
    if (![self.model isKindOfClass:[CPOrder class]]) {
        return;
    }
    
    CPOrder *order = (CPOrder *)self.model;
    
    _typeLabel.text = [NSString stringWithFormat:@"订单类型：%@", order.type == CPOrderTypePrint ? @"打印" : @"复印"];
    
    _locationLabel.text   = order.shop.name;
    
    _nickNameLabel.text = [NSString stringWithFormat:@"姓名：%@", order.nickName];
    _telLabel.text      = [NSString stringWithFormat:@"电话：%@", order.tel];
    _dormLabel.text     = [NSString stringWithFormat:@"宿舍：%@", order.dorm];
    
    _paidDateLabel.text   = [NSString stringWithFormat:@"支付时间：%@", order.paidDate];
    _demandDateLabel.text = [NSString stringWithFormat:@"期望时间：%@", order.demandDate];
    
    NSString *buttonTitle = order.state == CPOrderStatePurchased ? @"完成打印" : order.state == CPOrderStatePrinted ? @"完成配送" : @"已配送";
    
    [_handleButton setEnabled:order.state == CPOrderStatePrinted];
    [_handleButton setTitle:buttonTitle forState:UIControlStateNormal];
    
}

@end
