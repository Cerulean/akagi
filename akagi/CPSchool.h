//
//  CPSchool.h
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"
#import "CPBuilding.h"

@interface CPSchool : CPBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSArray<CPBuilding *> *buildings;

@end
