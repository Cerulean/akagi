//
//  CPCoding.h
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

@interface CPCoding : NSObject

+ (NSString *) md5:(NSString *)str;

@end
