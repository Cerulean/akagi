//
//  CPBuilding.m
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBuilding.h"

@implementation CPBuilding

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"     : @"ID",
             @"name"   : @"name",
             @"status" : @"status"};
}

@end
