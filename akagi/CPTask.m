//
//  CPTask.m
//  akagi
//
//  Created by 王澍宇 on 15/10/19.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPTask.h"

@implementation CPTask

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"         : @"ID",
             @"fileName"   : @"file_name",
             @"pages"      : @"pages",
             @"bothSide"   : @"bothside",
             @"colorfull"  : @"colorful",
             @"copies"     : @"copies"};
}

@end
