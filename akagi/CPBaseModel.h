//
//  CPBaseModel.h
//  akagi
//
//  Created by 王澍宇 on 15/10/9.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Mantle.h>

@interface CPBaseModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSInteger ID;

+ (NSDateFormatter *)dateFormatter;

@end
