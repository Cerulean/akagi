//
//  CPModifyInfoController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewController.h"
#import "CPTextFieldCell.h"
#import "CPTextLabelCell.h"
#import "CPManager.h"
#import "CPSchool.h"

/* Static Cell is not match for MVVM's viewModel */

@interface CPModifyInfoController : CPBaseViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) CPTextFieldCell *nameLabel;
@property (nonatomic, strong) CPTextLabelCell *sexLabel;
@property (nonatomic, strong) CPTextFieldCell *qqLabel;
@property (nonatomic, strong) CPTextFieldCell *emailLabel;

@property (nonatomic, strong) CPTextFieldCell *alipayLabel;
@property (nonatomic, strong) CPTextFieldCell *idCardLabel;


@property (nonatomic, strong) CPTextLabelCell *typeLabel;
@property (nonatomic, strong) CPTextLabelCell *schoolLabel;
@property (nonatomic, strong) CPTextFieldCell *graduateLabel;
@property (nonatomic, strong) CPTextLabelCell *buildingLabel;
@property (nonatomic, strong) CPTextLabelCell *manageBuildingsLabel;
@property (nonatomic, strong) CPTextFieldCell *dormLabel;

@property (nonatomic, strong) CPTextLabelCell *cardLabel;

@property (nonatomic, strong) CPManager *manager;

- (void)finishModify:(UIBarButtonItem *)sender;

@end
