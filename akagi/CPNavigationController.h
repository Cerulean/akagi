//
//  CPNavigationController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/9.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPBaseViewController.h"

@class CPBaseViewModel;

@class CPTabBarController;

@interface CPNavigationController : UINavigationController

@property (nonatomic, strong) CPTabBarController *tabController;

- (void)pushViewController:(Class)viewControllerClass ViewModel:(CPBaseViewModel *)viewModel animated:(BOOL)animated;

@end
