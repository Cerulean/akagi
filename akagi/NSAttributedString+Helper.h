//
//  NSAttributedString+Helper.h
//  akagi
//
//  Created by 王澍宇 on 15/11/4.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSAttributedString (Helper)

+ (NSAttributedString *)attributedStringWithString:(NSString *)string Color:(UIColor *)color;

+ (NSAttributedString *)attributedStringWithString:(NSString *)string Color:(UIColor *)color FontSize:(CGFloat)fontSize;

@end
