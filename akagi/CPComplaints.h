//
//  CPComplaints.h
//  akagi
//
//  Created by 王澍宇 on 15/11/6.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"

@interface CPComplaints : CPBaseModel

@property (nonatomic, strong) NSString *comment;

@property (nonatomic, strong) NSString *date;

@end
