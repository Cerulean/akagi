//
//  CPOrderDetailController.m
//  akagi
//
//  Created by 王澍宇 on 15/11/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderDetailController.h"
#import "CPOrderDetailView.h"

@interface CPOrderDetailController ()

@end

@implementation CPOrderDetailController {
    
    UIScrollView *_scrolView;
    
    UIView *_contentView;
    
    UIImageView *_fileIcon;
    
    UILabel *_fileLabel;
    
    UIView *_fileView;
    
    UILabel *_feeLabel;
    
    UIView *_leftLine;
    UIView *_rightLine;
    
    UILabel *_infoLabel;
    UILabel *_messageLabel;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"订单详情";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    WeakSelf;
    
    [self.viewModel fetchModelWithBlock:^(NSArray *objects, NSError *error) {
        
        CPOrderDetail *orderDetail = [objects firstObject];
        
        StrongSelf;
        
        s_self->_feeLabel.text     = [NSString stringWithFormat:@"合计%.2f元（含配送费%.2f元）", orderDetail.fee, orderDetail.deliverFee];
        s_self->_messageLabel.text = orderDetail.note;
        
        static const CGFloat DetailHeight = 25;
        
        for (CPTask *task in orderDetail.tasks) {
            
            NSUInteger index = [orderDetail.tasks indexOfObject:task];
            
            CPOrderDetailView *detailView = [CPOrderDetailView new];
            
            [detailView bindWithModel:task];
            
            [s_self->_fileView addSubview:detailView];
            
            [detailView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(s_self->_fileView).offset(index * DetailHeight);
                make.left.right.equalTo(@0);
                
                if (task == [orderDetail.tasks lastObject]) {
                    make.bottom.equalTo(@0);
                }
            }];
        }
    }];
    
    _scrolView = [UIScrollView new];
    
    _contentView = [UIView new];
    
    _fileIcon = [[UIImageView alloc] initWithImage:CPImageWithName(@"Order_File")];
    
    _fileLabel = [UILabel labelWithText:@"文件信息" Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentLeft];
    
    _feeLabel = [UILabel labelWithText:@"合计0.00元（含配送费0.00元）" Color:ColorSubTextContent FontSize:12 Alignment:NSTextAlignmentCenter];
    
    _fileView = [UIView new];
    
    _leftLine  = [UIView new];
    _leftLine.backgroundColor  = ColorMainTextContent;
    
    _rightLine = [UIView new];
    _rightLine.backgroundColor = ColorMainTextContent;
    
    _infoLabel = [UILabel labelWithText:@"留言" Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentCenter];
    
    _messageLabel = [UILabel labelWithText:nil Color:ColorSubTextContent FontSize:12 Alignment:NSTextAlignmentLeft];
    _messageLabel.numberOfLines = 0;
    
    [self.view addSubview:_scrolView];
    
    [_scrolView addSubview:_contentView];
    
    [_contentView addSubview:_fileIcon];
    [_contentView addSubview:_fileLabel];
    [_contentView addSubview:_fileView];
    [_contentView addSubview:_feeLabel];
    [_contentView addSubview:_leftLine];
    [_contentView addSubview:_rightLine];
    [_contentView addSubview:_infoLabel];
    [_contentView addSubview:_messageLabel];
    
    [_scrolView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(@0);
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@0);
        make.width.equalTo(@(SCREEN_WIDTH));
    }];
    
    [_fileIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(@10);
    }];
    
    [_fileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_fileIcon.mas_right).offset(5);
        make.centerY.equalTo(_fileIcon);
    }];
    
    [_fileView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_fileIcon.mas_bottom).offset(20);
        make.left.equalTo(@20);
        make.right.equalTo(@(-20));
    }];
    
    [_feeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_fileView.mas_bottom).offset(20);
        make.centerX.equalTo(@0);
    }];
    
    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(_feeLabel.mas_bottom).offset(50);
    }];
    
    [_leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.centerY.equalTo(_infoLabel);
        make.right.equalTo(_infoLabel.mas_left).offset(-20);
        make.height.equalTo(@0.5);
    }];
    
    [_rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_leftLine);
        make.right.equalTo(@(-20));
        make.size.equalTo(_leftLine);
    }];
    
    [_messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(_infoLabel.mas_bottom).offset(20);
        make.right.equalTo(@(-20));
        make.bottom.equalTo(@(-20));
    }];
}

@end
