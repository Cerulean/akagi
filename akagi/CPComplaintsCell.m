//
//  CPComplainsCell.m
//  akagi
//
//  Created by 王澍宇 on 15/11/6.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPComplaintsCell.h"

@implementation CPComplaintsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        _complaintsLabel = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentLeft];
        _complaintsLabel.numberOfLines = 0;
        
        _dateLabel       = [UILabel labelWithText:nil Color:ColorSubTextContent  FontSize:12 Alignment:NSTextAlignmentRight];
        
        [self.contentView addSubview:_complaintsLabel];
        [self.contentView addSubview:_dateLabel];
        
        [_complaintsLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@10);
            make.left.equalTo(@20);
            make.right.equalTo(@(-20));
        }];
        
        [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_complaintsLabel.mas_bottom).offset(20);
            make.right.equalTo(@(-10));
            make.bottom.equalTo(@(-10));
        }];
    }
    return self;
}

- (void)bindWithModel:(CPBaseModel *)model {
    
    self.model = model;
    
    if (![self.model isKindOfClass:[CPComplaints class]]) {
        return;
    }
    
    CPComplaints *complaints = (CPComplaints *)self.model;
    
    _complaintsLabel.text = complaints.comment;
    _dateLabel.text       = complaints.date;
}

@end
