//
//  CPColor.m
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPColor.h"

@implementation CPColor

+ (UIColor *)colorWithHex:(UInt32)hex {
    return [UIColor colorWithHex:hex];
}

+ (UIColor *)colorWithHex:(UInt32)hex andAlpha:(CGFloat)alpha {
    return [UIColor colorWithHex:hex andAlpha:alpha];
}

@end
