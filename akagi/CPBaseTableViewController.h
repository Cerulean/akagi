//
//  CPBaseTableViewController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <MJRefresh.h>

#import "CPBaseViewController.h"

@interface CPBaseTableViewController : CPBaseViewController <UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) BOOL enableUpRefresh;

@property (nonatomic, assign) BOOL enableDownRrefresh;

@end
