//
//  CPPasswordController.m
//  akagi
//
//  Created by 王澍宇 on 15/11/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPPasswordController.h"

@implementation CPPasswordController {
    UITextField *_oldPasswordField;
    UITextField *_newPasswordFiled;
    UITextField *_conPasswordFiled;
    
    UIView *_firstLine;
    UIView *_secondLine;
    UIView *_thirdLine;
}

- (instancetype)init {
    if (self = [super init]) {
        self.title = @"修改密码";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    WeakSelf;
    
    _oldPasswordField = [UITextField new];
    _oldPasswordField.secureTextEntry = YES;
    _oldPasswordField.returnKeyType = UIReturnKeyNext;
    _oldPasswordField.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"旧密码" Color:ColorPlacehodler];
    _oldPasswordField.doneAction = ^(NSString *text) {
        
        StrongSelf;
        
        [s_self->_oldPasswordField resignFirstResponder];
        [s_self->_newPasswordFiled becomeFirstResponder];
    };
    
    _newPasswordFiled = [UITextField new];
    _newPasswordFiled.secureTextEntry = YES;
    _newPasswordFiled.returnKeyType = UIReturnKeyNext;
    _newPasswordFiled.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"新密码" Color:ColorPlacehodler];
    _newPasswordFiled.doneAction = ^(NSString *text) {
        
        StrongSelf;
        
        [s_self->_newPasswordFiled resignFirstResponder];
        [s_self->_conPasswordFiled becomeFirstResponder];
    };
    
    _conPasswordFiled = [UITextField new];
    _conPasswordFiled.secureTextEntry = YES;
    _conPasswordFiled.returnKeyType = UIReturnKeyDone;
    _conPasswordFiled.attributedPlaceholder = [NSAttributedString attributedStringWithString:@"确认新密码" Color:ColorPlacehodler];
    _conPasswordFiled.doneAction = ^(NSString *text) {
        
        StrongSelf;
        
        NSString *oldPassword = s_self->_oldPasswordField.text;
        NSString *newPassword = s_self->_newPasswordFiled.text;
        NSString *conPassword = s_self->_conPasswordFiled.text;
        
        if (![newPassword isEqualToString:conPassword]) {
            [CPAlert alertWithTitle:@"错误" Message:@"两次输入的新密码不相同"];
            return;
        }
        
        if ([oldPassword isEqualToString:conPassword]) {
            [CPAlert alertWithTitle:@"错误" Message:@"新旧密码不能相同"];
            return;
        }
        
        if (![CPValidater validatePassword:oldPassword] || ![CPValidater validatePassword:newPassword]) {
            [CPAlert alertWithTitle:@"错误" Message:@"新或旧密码不合法"];
            return;
        }
        
        NSDictionary *parms = @{@"oldPassword" : [CPCoding md5:oldPassword],
                                @"newPassword" : [CPCoding md5:newPassword]};
        
        [CPWebService commonRequestWithAPI:@"manager/password/update" Method:@"POST" Parms:[parms copy] Block:^(BOOL success, NSDictionary *resultDic) {
            if (success) {
                [s_self.navigationController popViewControllerAnimated:YES];
            }
        }];
    };
    
    _firstLine = [UIView new];
    _firstLine.backgroundColor = ColorPlacehodler;
    
    _secondLine = [UIView new];
    _secondLine.backgroundColor = ColorPlacehodler;
    
    _thirdLine = [UIView new];
    _thirdLine.backgroundColor = ColorPlacehodler;
    
    [self.view addSubview:_oldPasswordField];
    [self.view addSubview:_newPasswordFiled];
    [self.view addSubview:_conPasswordFiled];
    
    [self.view addSubview:_firstLine];
    [self.view addSubview:_secondLine];
    [self.view addSubview:_thirdLine];
    
    [_oldPasswordField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@100);
        make.left.equalTo(@20);
        make.right.equalTo(@(-30));
    }];
    
    [_newPasswordFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_oldPasswordField.mas_bottom).offset(40);
        make.left.equalTo(@20);
        make.right.equalTo(@(-30));
    }];
    
    [_conPasswordFiled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_newPasswordFiled.mas_bottom).offset(34);
        make.left.equalTo(@20);
        make.right.equalTo(@(-30));
    }];
    
    [_firstLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_oldPasswordField.mas_bottom).offset(10);
        make.left.right.equalTo(_oldPasswordField);
        make.height.equalTo(@0.5);
    }];
    
    [_secondLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_newPasswordFiled.mas_bottom).offset(10);
        make.left.right.equalTo(_oldPasswordField);
        make.height.equalTo(_firstLine);
    }];
    
    [_thirdLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_conPasswordFiled.mas_bottom).offset(10);
        make.left.right.equalTo(_oldPasswordField);
        make.height.equalTo(_firstLine);
    }];
}

@end
