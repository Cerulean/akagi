//
//  CPBaseViewModel.h
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "CPNetworkEngine.h"
#import "CPBaseCell.h"

typedef void(^CPFetchModelBlock)(NSArray *objects, NSError *error);

@interface CPBaseViewModel : NSObject <UITableViewDataSource>

@property (nonatomic, copy) NSString *api;

@property (nonatomic, strong) NSMutableDictionary *prams;

@property (nonatomic, strong) CPBaseModel *model;

@property (nonatomic, strong) Class cellClass;

@property (nonatomic, strong) NSMutableArray *objects;

- (instancetype)initWithAPI:(NSString *)api;

- (instancetype)initWithAPI:(NSString *)api CellClass:(Class)cellClass;

- (void)fetchModelWithBlock:(CPFetchModelBlock)block;

- (void)retryFetchModel;

- (void)fetchMoreModelWithBlock:(CPFetchModelBlock)block;

- (void)retryFetchMoreModel;

- (void)fetchClearWithBlock:(CPFetchModelBlock)block;

- (void)removeObject:(CPBaseModel *)object Block:(void (^)())block;

- (CPBaseModel *)objectWithID:(NSInteger)ID;

@end
