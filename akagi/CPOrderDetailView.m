//
//  OrderDetailView.m
//  akagi
//
//  Created by 王澍宇 on 15/11/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderDetailView.h"
#import "CPCommonTools.h"

@implementation CPOrderDetailView

- (instancetype)init {
    if (self = [super init]) {
        
        _icon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Order_Dot"]];
        
        _fileNameLabel = [UILabel labelWithText:nil Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentLeft];
        
        _sideLabel   = [UILabel labelWithText:nil Color:ColorSubTextContent FontSize:12 Alignment:NSTextAlignmentRight];
        
        _colorLabel  = [UILabel labelWithText:nil Color:ColorSubTextContent FontSize:12 Alignment:NSTextAlignmentRight];
        
        _copiesLabel = [UILabel labelWithText:nil Color:ColorSubTextContent FontSize:12 Alignment:NSTextAlignmentRight];
        
        _pageLabel   = [UILabel labelWithText:nil Color:ColorSubTextContent FontSize:12 Alignment:NSTextAlignmentRight];
        
        [self addSubview:_icon];
        [self addSubview:_fileNameLabel];
        [self addSubview:_sideLabel];
        [self addSubview:_colorLabel];
        [self addSubview:_copiesLabel];
        [self addSubview:_pageLabel];
        
        [_icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.centerY.equalTo(_fileNameLabel);
        }];
        
        [_fileNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(_icon.mas_right).offset(10);
            make.top.equalTo(@(5));
            make.bottom.equalTo(@(-5));
            make.width.lessThanOrEqualTo(@150);
        }];
        
        [_pageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_copiesLabel.mas_left).offset(-10);
            make.centerY.equalTo(_fileNameLabel);
        }];
        
        [_copiesLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_colorLabel.mas_left).offset(-10);
            make.centerY.equalTo(_fileNameLabel);
        }];
        
        [_colorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(_sideLabel.mas_left).offset(-10);
            make.centerY.equalTo(_fileNameLabel);
        }];
        
        [_sideLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_fileNameLabel);
            make.right.equalTo(@0);
        }];
    }
    return self;
}

- (void)bindWithModel:(CPTask *)model {
    if (![model isKindOfClass:[CPTask class]]) {
        return;
    }
    
    _fileNameLabel.text = model.fileName;
    _sideLabel.text     = model.bothSide ? @"双面" : @"单面";
    _colorLabel.text    = model.colorful ? @"彩印" : @"黑白";
    _copiesLabel.text   = [NSString stringWithFormat:@"%ld份", (long)model.copies];
    _pageLabel.text     = [NSString stringWithFormat:@"%ld页", (long)model.pages];
}

@end
