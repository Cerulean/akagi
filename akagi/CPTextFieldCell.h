//
//  CPTextFieldCell.h
//  akagi
//
//  Created by 王澍宇 on 15/10/14.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseCell.h"

@interface CPTextFieldCell : CPBaseCell

@property (nonatomic, strong, readonly) UILabel *titleLabel;

@property (nonatomic, strong, readonly) UITextField *textField;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *text;

@end
