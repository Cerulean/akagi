//
//  CPBaseViewModel.m
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewModel.h"

@implementation CPBaseViewModel {
    CPFetchModelBlock _fetchBlock;
    CPFetchModelBlock _fetchMoreBlock;
}

- (instancetype)initWithAPI:(NSString *)api {
    if (self = [super init]) {
        _api     = api;
        _prams   = [NSMutableDictionary dictionary];
        _objects = [NSMutableArray array];
    }
    return self;
}

- (instancetype)initWithAPI:(NSString *)api CellClass:(Class)cellClass {
    if (self = [self initWithAPI:api]) {
        _cellClass = cellClass;
    }
    return self;
}

- (void)fetchModelWithBlock:(CPFetchModelBlock)block {
    
    _fetchBlock = [block copy];
    
    [self fetchModelWithBlock:_fetchBlock NeedMore:NO NeedClear:NO];
}

- (void)fetchMoreModelWithBlock:(CPFetchModelBlock)block {
    
    _fetchMoreBlock = [block copy];
    
    [self fetchModelWithBlock:block NeedMore:YES NeedClear:NO];
}

- (void)fetchClearWithBlock:(CPFetchModelBlock)block {
    
    _fetchBlock = [block copy];
    
    [self fetchModelWithBlock:_fetchBlock NeedMore:NO NeedClear:YES];
}

- (void)retryFetchModel {
    [self fetchModelWithBlock:_fetchBlock NeedMore:NO NeedClear:NO];
}

- (void)retryFetchMoreModel {
    [self fetchModelWithBlock:_fetchMoreBlock NeedMore:YES NeedClear:NO];
}

- (void)removeObject:(CPBaseModel *)object Block:(void (^)())block {
    CPBaseModel *flagModel = nil;
    
    flagModel = [self objectWithID:object.ID];
    
    if (flagModel) {
        [_objects removeObject:flagModel];
        
        if (block) {
            block();
        }
    }
}

#pragma mark - Private Method 

- (CPBaseModel *)objectWithID:(NSInteger)ID {
    for (CPBaseModel *model in _objects) {
        if (model.ID ==ID) {
            return model;
        }
    }
    return nil;
}

- (void)fetchModelWithBlock:(CPFetchModelBlock)block NeedMore:(BOOL)needMore NeedClear:(BOOL)needClear {
    
    NSMutableDictionary *parms = [[NSMutableDictionary alloc] initWithDictionary:_prams];
    
    if (needMore) {
        parms[@"lastID"] = @([(CPBaseModel *)[_objects lastObject] ID]);
    }
    
    WeakSelf;
    
    [[CPNetworkEngine sharedEngine] fetchModelWithAPI:_api Parms:parms Callback:^(NSArray *objects, NSError *error) {
        
        if (!error) {
            
            StrongSelf;
            
            if (s_self && objects.count) {
                
                if (needMore) {
                    [s_self.objects addObjectsFromArray:objects];
                } else {
                    if (needClear) {
                        [s_self.objects removeAllObjects];
                        [s_self.objects addObjectsFromArray:objects];
                    } else {
                        [s_self.objects insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, objects.count)]];
                    }
                }
            }
            
            if (block) {
                block(objects, nil);
            }
            
        } else {
            if (block) {
                block(nil, error);
            }
        }
        
    }];
}

#pragma mark - TableView DataSource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reuseIdentifier = @"ReuseIdentifier";
    
    CPBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    if (!cell) {
        cell = [[_cellClass alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        cell.tableView = tableView;
    }
    
    [cell bindWithModel:_objects[indexPath.row]];
    
    return cell;
    
}

@end
