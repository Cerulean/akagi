//
//  CPBaseModel.m
//  akagi
//
//  Created by 王澍宇 on 15/10/9.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"

@implementation CPBaseModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID" : @"ID"};
}

+ (NSDateFormatter *)dateFormatter {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"MM-dd HH:mm";
    return dateFormatter;
}


@end
