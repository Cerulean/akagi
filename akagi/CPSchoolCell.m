//
//  CPSearchResultCell.m
//  akagi
//
//  Created by 王澍宇 on 15/10/29.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPSchoolCell.h"

@implementation CPSchoolCell

- (void)bindWithModel:(CPBaseModel *)model {
    if ([model isKindOfClass:[CPSchool class]]) {
        CPSchool *school = (CPSchool *)model;
        
        self.textLabel.text = school.name;
    }
}

@end
