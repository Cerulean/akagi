//
//  CPOrder.h
//  akagi
//
//  Created by 王澍宇 on 15/10/18.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"
#import "CPShop.h"

typedef enum : NSUInteger {
    CPOrderStatePurchased,
    CPOrderStatePrinted,
    CPOrderStateDeliverd,
} CPOrderState;

typedef enum : NSUInteger {
    CPOrderTypePrint,
    CPOrderTypeCopy,
} CPOrderType;

@interface CPOrder : CPBaseModel

@property (nonatomic, strong) NSString *tradeID;

@property (nonatomic, assign) CPOrderState state;

@property (nonatomic, assign) CPOrderType type;

@property (nonatomic, strong) NSString *dorm;

@property (nonatomic, strong) NSString *tel;

@property (nonatomic, strong) NSString *nickName;

@property (nonatomic, strong) NSString *paidDate;

@property (nonatomic, strong) NSString *demandDate;

@property (nonatomic, strong) NSString *deliveredDate;

@property (nonatomic, assign) NSInteger rate;

@property (nonatomic, strong) CPShop *shop;

@end
