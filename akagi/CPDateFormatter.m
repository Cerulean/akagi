//
//  CPDateFormatter.m
//  akagi
//
//  Created by 王澍宇 on 15/10/19.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPDateFormatter.h"

@implementation CPDateFormatter

+ (NSDate *)dateFromString:(NSString *)dateString {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    
    return destDate;
    
}

+ (NSString *)stringFromDate:(NSDate *)date {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"MM-dd HH:mm"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    return destDateString;
    
}

+ (NSString *)stringFromValue:(NSString *)value {
    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[value longLongValue]];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm"];
    
    NSString *date = [formatter stringFromDate:confromTimesp];
    return date;
}

@end
