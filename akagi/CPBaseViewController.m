//
//  CPBaseViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewController.h"

@implementation CPBaseViewController {
    BOOL _isViewDidAppearFirstTime;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isViewDidAppearFirstTime = YES;
    
    UIBarButtonItem *leftItem = nil;
    
    if ([self.navigationController.viewControllers count] > 1) {
        leftItem = [CPBarButtonItem itemWithTitle:@"返回" Color:ColorMainTextContent Target:self Action:@selector((goBack))];
    } else {
        if (self.presentingViewController) {
            leftItem = [CPBarButtonItem itemWithTitle:@"返回" Color:ColorMainTextContent Target:self Action:@selector((dismiss))];
        }
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationItem.leftBarButtonItem = leftItem;
    
    if (self.navigationController.navigationBar.isHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}

- (void)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
