//
//  OrderDetailView.h
//  akagi
//
//  Created by 王澍宇 on 15/11/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPOrderDetail.h"

@interface CPOrderDetailView : UIView

@property (nonatomic, strong) UIImageView *icon;

@property (nonatomic, strong) UILabel *fileNameLabel;

@property (nonatomic, strong) UILabel *pageLabel;

@property (nonatomic, strong) UILabel *copiesLabel;

@property (nonatomic, strong) UILabel *colorLabel;

@property (nonatomic, strong) UILabel *sideLabel;

- (void)bindWithModel:(CPTask *)model;

@end
