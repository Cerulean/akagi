//
//  CPSearchViewController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/15.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseTableViewController.h"

typedef void(^CPSearchResultAction)(CPBaseModel *model);

@interface CPSearchViewController : CPBaseTableViewController <UISearchBarDelegate>

@property (nonatomic, strong) CPSearchResultAction action;

@end
