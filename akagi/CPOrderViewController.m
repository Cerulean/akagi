//
//  CPOrderViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderViewController.h"
#import "CPOrderDetailController.h"

@interface CPOrderViewController ()

@end

@implementation CPOrderViewController

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    CPBaseViewModel *viewModel = [[CPBaseViewModel alloc] initWithAPI:@"orders/detail"];
    viewModel.prams[@"ID"] = @([(CPBaseModel *)[self.viewModel objects][indexPath.row] ID]);
    
    CPOrderDetailController *orderDetailController = [CPOrderDetailController new];
    orderDetailController.viewModel = viewModel;
    
    [self.navigationController pushViewController:orderDetailController animated:YES];
}

@end
