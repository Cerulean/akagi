//
//  CPCardPickerController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPCardPickerController.h"

@interface CPCardPickerController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end

@implementation CPCardPickerController {
    UIImageView *_studentCardImageView;
    
    UILabel *_firstLabel;
    
    UIView *_line;
    
    UIImagePickerController *_imagePickerController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *rightItem = [CPBarButtonItem itemWithTitle:@"完成"
                                                          Color:ColorMainTextContent
                                                         Target:self
                                                         Action:@selector(rightItemAction)];
    
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _imagePickerController = [[UIImagePickerController alloc] init];
    _imagePickerController.delegate = self;
    
    [self setupScrollView];
}

- (void)setupScrollView {
    
    _firstLabel = [UILabel labelWithText:@"上传一卡通或者学生证正面" Color:ColorMainTextContent FontSize:14 Alignment:NSTextAlignmentCenter];
    
    WeakSelf;

    _studentCardImageView = [[UIImageView alloc] init];
    _studentCardImageView.backgroundColor = ColorSubTextContent;
    _studentCardImageView.userInteractionEnabled = YES;
    _studentCardImageView.tapAction = ^(UIView *view) {
        StrongSelf;
        [s_self pickImageWithImageView:(UIImageView *)view];
    };
    
    _line = [UIView new];
    _line.backgroundColor = ColorMainTextContent;
    
    [self.view addSubview:_firstLabel];
    [self.view addSubview:_line];
    [self.view addSubview:_studentCardImageView];
    
    [_firstLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@20);
        make.top.equalTo(@50);
        make.right.equalTo(@(-20));
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@35);
        make.right.equalTo(@(-35));
        make.top.equalTo(_firstLabel.mas_bottom).offset(20);
    }];
    
    [_studentCardImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@(20));
        make.right.equalTo(@(-20));
        make.top.equalTo(_line.mas_bottom).offset(35);
        make.bottom.equalTo(@(-100));
    }];
}

- (void)pickImageWithImageView:(UIImageView *)imageView {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"请选择上传方式"
                                                                             message:nil
                                                                      preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancelActinon = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    
    WeakSelf;
    
    UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        StrongSelf;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            [_imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
            [s_self presentViewController:_imagePickerController animated:YES completion:nil];
            
        }
    }];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        StrongSelf;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [_imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
            [s_self presentViewController:_imagePickerController animated:YES completion:nil];
            
        }
    }];
    
    [alertController addAction:cancelActinon];
    [alertController addAction:albumAction];
    [alertController addAction:cameraAction];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)rightItemAction {
    
    if (!_studentCardImageView.image) {
        [CPAlert alertWithTitle:@"错误" Message:@"学生证一卡通或身份证未上传"];
        
        return;
    }
    
    if (_block) {
        _block(_studentCardImageView.image);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ImagePickerController Delegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    _studentCardImageView.image = info[UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}



@end
