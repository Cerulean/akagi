//
//  CPWebService.h
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CPNetworkEngine.h"

#import "CPManager.h"

@interface CPWebService : NSObject

+ (void)logOut;

+ (void)checkIfNeedFinishInfoWithBlock:(void (^)(BOOL need))block;

+ (void)checkIfNeedLoginWithBlock:(void (^)(BOOL need))block;

+ (void)loginWithParms:(NSDictionary *)parms Block:(void (^)(BOOL success))block;

+ (void)signUpWithParms:(NSDictionary *)parms Block:(void (^)(BOOL success))block;

+ (void)resignWithBlock:(void (^)(BOOL success))block;

+ (void)commonRequestWithAPI:(NSString *)api Method:(NSString *)method Parms:(NSDictionary *)parms Block:(void (^)(BOOL success, NSDictionary *resultDic))block;

+ (CPManagerState)managerState;

+ (void)setManagerState:(CPManagerState)managerState;

@end
