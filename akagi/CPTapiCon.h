//
//  CPVerticalButton.h
//  akagi
//
//  Created by 王澍宇 on 15/11/5.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>

@interface CPTapiCon : UIView

@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIColor *color;

@property (nonatomic, strong) UIColor *selectedColor;

@property (nonatomic, assign) BOOL selected;

+ (instancetype)iConWithTitle:(NSString *)title
                     FontSize:(CGFloat)fontSize
                        Image:(UIImage *)image
                SelectedImage:(UIImage *)selectedImage
                        Color:(UIColor *)color
                SelectedColor:(UIColor *)selectedColor;

@end
