//
//  CPShop.h
//  akagi
//
//  Created by 王澍宇 on 15/11/7.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"

@interface CPShop : CPBaseModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *location;

@property (nonatomic, strong) NSString *tel;

@end
