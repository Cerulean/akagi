//
//  AppDelegate.m
//  akagi
//
//  Created by 王澍宇 on 15/9/21.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "AppDelegate.h"

#import "CPTabBarController.h"

#import "CPUserViewController.h"
#import "CPOrderViewController.h"
#import "CPModifyInfoController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Setup AVOS
    
    [AVOSCloud setApplicationId:@"IkS2S5HlwHOTmGCmLU4rXgSL" clientKey:@"oXPgIIbidIMvkCseQgcb6eJS"];
    
    // Setup Remote Notifications
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|
                                                                                        UIUserNotificationTypeBadge|
                                                                                        UIUserNotificationTypeSound
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    // Setup Window and RootViewController
    
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    CPOrderViewController *orderViewController = [[CPOrderViewController alloc] init];
    orderViewController.title = @"订单";
    
    CPBaseViewModel *orderViewModel = [[CPBaseViewModel alloc] initWithAPI:@"manager/orders" CellClass:[CPOrderCell class]];
    
    [orderViewController setViewModel:orderViewModel];
    
    CPNavigationController *orderNavigationController = [[CPNavigationController alloc] initWithRootViewController:orderViewController];
    
    CPUserViewController *userViewController = [[CPUserViewController alloc] init];
    userViewController.title = @"个人";
    
    CPNavigationController *userNavigationController = [[CPNavigationController alloc] initWithRootViewController:userViewController];
    
    CPTabBarController *tabBarController = [[CPTabBarController alloc] initWithViewControllers:@[orderNavigationController, userNavigationController]
                                                                                        Titles:@[@"订单", @"个人"]
                                                                                        Images:@[CPImageWithName(@"Tab_Order"),
                                                                                                 CPImageWithName(@"Tab_User")]
                                                                                SelectedImages:@[CPImageWithName(@"Tab_Order_Selected"),
                                                                                                 CPImageWithName(@"Tab_User_Selected")]];
    [self.window setRootViewController:tabBarController];
    [self.window makeKeyAndVisible];
    
    [[UITabBar appearance] setBarTintColor:ColorWhite];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorMainTextContent}
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorSubTextContent}
                                             forState:UIControlStateSelected];
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:ColorWhite];
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : ColorMainTextContent}];
    
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [CPWebService checkIfNeedLoginWithBlock:^(BOOL need) {
            if (!need) {
                [CPWebService checkIfNeedFinishInfoWithBlock:nil];
            }
        }];
    });
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    // Setup AVOS Installation
    
    AVInstallation *currentInstallation = [AVInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
    
    NSString *deviceTokenKey = [NSString stringWithFormat:@"%@", deviceToken];
    
    [[CPNetworkEngine sharedEngine] setDeviceToken:deviceTokenKey];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
