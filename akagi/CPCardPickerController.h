//
//  CPCardPickerController.h
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseViewController.h"

typedef void(^CPCardPickResultBlock)(UIImage *firstImage);

@interface CPCardPickerController : CPBaseViewController

@property (nonatomic, strong) CPCardPickResultBlock block;

@end
