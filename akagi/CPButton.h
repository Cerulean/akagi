//
//  CPButton.h
//  akagi
//
//  Created by 王澍宇 on 15/10/13.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPButton;

typedef void(^CPButtonActionBlock)(CPButton *button);

@interface CPButton : UIButton

@property (nonatomic, strong) CPButtonActionBlock actionBlock;

+ (instancetype)buttonWithType:(UIButtonType)buttonType ActionBlock:(CPButtonActionBlock)actionBlock;

+ (instancetype)buttonWithType:(UIButtonType)buttonType Title:(NSString *)title FontSize:(CGFloat)fontSize ActionBlock:(CPButtonActionBlock)actionBlock;


@end
