//
//  CPOrderDetail.h
//  akagi
//
//  Created by 王澍宇 on 15/11/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseModel.h"
#import "CPTask.h"

@interface CPOrderDetail : CPBaseModel

@property (nonatomic, assign) float fee;

@property (nonatomic, assign) float deliverFee;

@property (nonatomic, assign) NSInteger rate;

@property (nonatomic, strong) NSString *note;

@property (nonatomic, strong) NSString *comment;

@property (nonatomic, strong) NSArray<CPTask *> *tasks;

@end
