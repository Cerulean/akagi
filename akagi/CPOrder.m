//
//  CPOrder.m
//  akagi
//
//  Created by 王澍宇 on 15/10/18.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrder.h"

@implementation CPOrder

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"         : @"order_id",
             @"tradeID"    : @"trade_no",
             @"state"      : @"order_state",
             @"dorm"       : @"student_room",
             @"tel"        : @"student_phone",
             @"nickName"   : @"nickname",
             @"paidDate"   : @"paid_at",
             @"demandDate" : @"demand_time"};
}

+ (NSValueTransformer *)paidDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString * dateString, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [self.dateFormatter dateFromString:dateString];
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)demandDateJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSString * dateString, BOOL *success, NSError *__autoreleasing *error) {
        NSDate *date = [self.dateFormatter dateFromString:dateString];
        return [self.dateFormatter stringFromDate:date];
    }];
}



@end
