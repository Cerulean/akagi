//
//  CPBaseTableViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseTableViewController.h"
#import "CPRefreshHeader.h"

@implementation CPBaseTableViewController {
    
}

- (instancetype)init {
    if (self = [super init]) {
        _enableUpRefresh    = YES;
        _enableDownRrefresh = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    _tableView.dataSource      = self.viewModel;
    _tableView.delegate        = self;
    _tableView.rowHeight       = UITableViewAutomaticDimension;
    _tableView.contentInset    = UIEdgeInsetsMake(0, 0, STATUS_HEIGHT + TABBAR_HEIGHT, 0);
    _tableView.separatorStyle  = UITableViewCellSeparatorStyleNone;
    
    [self.view addSubview:_tableView];
    
    WeakSelf;
    
    if (_enableUpRefresh) {
        _tableView.header = [CPRefreshHeader headerWithRefreshingBlock:^{
            StrongSelf;
            
            [s_self.viewModel fetchModelWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    [s_self.tableView reloadData];
                }
                [s_self.tableView.header endRefreshing];
            }];
        }];
    }
    
    if (_enableDownRrefresh) {
        _tableView.footer = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
            StrongSelf;
            
            [s_self.viewModel fetchMoreModelWithBlock:^(NSArray *objects, NSError *error) {
                if (!error) {
                    [s_self.tableView reloadData];
                }
            }];
        }];
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return 70;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
