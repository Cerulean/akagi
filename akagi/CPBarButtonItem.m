//
//  CPBarButtonItem.m
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBarButtonItem.h"

@implementation CPBarButtonItem

+ (UIBarButtonItem *)itemWithTitle:(NSString *)title Color:(UIColor *)color Target:(id)target Action:(SEL)action {
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:target action:action];
    item.tintColor = color;
    return item;
}

@end
