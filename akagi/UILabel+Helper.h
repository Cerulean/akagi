//
//  UILabel+Helper.h
//  akagi
//
//  Created by 王澍宇 on 15/11/4.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Helper)

+ (UILabel *)labelWithText:(NSString *)text Color:(UIColor *)color FontSize:(CGFloat)fontSize Alignment:(NSTextAlignment)alignment;

@end
