//
//  CPSchool.m
//  akagi
//
//  Created by 王澍宇 on 15/10/16.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPSchool.h"

@implementation CPSchool

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"        : @"ID",
             @"name"      : @"name",
             @"buildings" : @"buildings"};
}

+ (NSValueTransformer *)buildingsJSONTransformer {
    return [MTLJSONAdapter arrayTransformerWithModelClass:[CPBuilding class]];
}

@end
