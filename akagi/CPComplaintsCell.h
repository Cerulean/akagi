//
//  CPComplainsCell.h
//  akagi
//
//  Created by 王澍宇 on 15/11/6.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseCell.h"

#import "CPComplaints.h"

@interface CPComplaintsCell : CPBaseCell

@property (nonatomic, strong) UILabel *complaintsLabel;

@property (nonatomic, strong) UILabel *dateLabel;

@end
