//
//  CPOrderDetail.m
//  akagi
//
//  Created by 王澍宇 on 15/11/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPOrderDetail.h"

@implementation CPOrderDetail

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"ID"         : @"ID",
             @"fee"        : @"fee",
             @"deliverFee" : @"deliver_fee",
             @"rate"       : @"rate",
             @"note"       : @"note",
             @"comment"    : @"comment",
             @"tasks"      : @"tasks"};
}

@end
