//
//  CPUserViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/20.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPUserViewController.h"
#import "CPInfoViewController.h"
#import "CPPasswordController.h"
#import "CPOrderViewController.h"
#import "CPComplaintsController.h"

@implementation CPUserViewController {
    NSArray *_firstCells;
    NSArray *_secondCells;
    NSArray *_thirdCells;
    
    NSArray *_cellsArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *rightItem = [CPBarButtonItem itemWithTitle:@"登出"
                                                          Color:ColorWarning
                                                         Target:self
                                                         Action:@selector(itemAction:)];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    _tableView.delegate   = self;
    _tableView.dataSource = self;
    _tableView.contentInset   = UIEdgeInsetsMake(0, 0, TABBAR_HEIGHT + STATUS_HEIGHT, 0);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = ColorSection;
    
    [self.view addSubview:_tableView];
    
    WeakSelf;
    
    _infoCell = [CPTextLabelCell new];
    _infoCell.title = @"修改信息";
    _infoCell.contentView.tapAction = ^(UIView *view) {
        
        StrongSelf;
        
        CPInfoViewController *infoController = [CPInfoViewController new];
        infoController.title = @"修改信息";
        infoController.viewModel = [[CPBaseViewModel alloc] initWithAPI:@"manager/info"];
        
        [s_self.navigationController pushViewController:infoController animated:YES];
    };
    
    _passwordCell = [CPTextLabelCell new];
    _passwordCell.title = @"修改密码";
    _passwordCell.contentView.tapAction = ^(UIView *view) {
        
        StrongSelf;
        
        CPPasswordController *passwordController = [CPPasswordController new];
        
        [s_self.navigationController pushViewController:passwordController animated:YES];
    };
    
    _ordersCell = [CPTextLabelCell new];
    _ordersCell.title = @"历史订单";
    _ordersCell.contentView.tapAction = ^(UIView *view) {
        
        StrongSelf;
        
        CPOrderViewController *orderViewController = [CPOrderViewController new];
        orderViewController.title = @"历史订单";
        orderViewController.viewModel = [[CPBaseViewModel alloc] initWithAPI:@"manager/orders" CellClass:[CPOrderCell class]];
        orderViewController.viewModel.prams[@"type"] = @"deliverd";
        
        [s_self.navigationController pushViewController:orderViewController animated:YES];
    };
    
    _complainCell = [CPTextLabelCell new];
    _complainCell.title = @"查看投诉";
    _complainCell.contentView.tapAction = ^(UIView *view) {
        
        StrongSelf;
        
        CPComplaintsController *complaintsController = [CPComplaintsController new];
        complaintsController.title = @"投诉";
        
        CPBaseViewModel *viewModel = [[CPBaseViewModel alloc] initWithAPI:@"shop/complainst" CellClass:[CPComplaintsCell class]];
        
        complaintsController.viewModel = viewModel;
        
        [s_self.navigationController pushViewController:complaintsController animated:YES];
    };
    
    _resignCell = [CPTextLabelCell new];
    _resignCell.title = @"辞职";
    _resignCell.titleLabel.textColor = ColorWarning;
    _resignCell.contentView.tapAction = ^(UIView *view) {
        [CPAlert alertWithTitle:@"提示" Message:@"确认提交辞职申请吗？" Stressed:YES confirmAction:^{
            [CPWebService resignWithBlock:nil];
        }];
    };
    
    _passwordCell.line.hidden = YES;
    _complainCell.line.hidden = YES;
    _resignCell.line.hidden   = YES;

    _firstCells  = @[_infoCell, _passwordCell];
    _secondCells = @[_ordersCell, _complainCell];
    _thirdCells  = @[_resignCell];
    
    _cellsArray = @[_firstCells, _secondCells, _thirdCells];
}

- (void)itemAction:(id)sender {
    [CPAlert alertWithTitle:@"登出" Message:@"确认登出？" Stressed:YES confirmAction:^{
        [CPWebService logOut];
    }];
}

#pragma mark - TableView Delegate && DataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == _cellsArray.count - 1) {
        return 50;
    } else {
        return 70;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _cellsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cellsArray[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return _cellsArray[indexPath.section][indexPath.row];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
