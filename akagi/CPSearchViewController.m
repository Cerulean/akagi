//
//  CPSearchViewController.m
//  akagi
//
//  Created by 王澍宇 on 15/10/15.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPSearchViewController.h"

@implementation CPSearchViewController {
    UISearchBar *_searchBar;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, COMMON_HEIGHT)];
    _searchBar.placeholder = @"搜索学校";
    _searchBar.delegate = self;
    
    self.tableView.tableHeaderView = _searchBar;
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 300, 0);
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    self.viewModel.prams[@"keyword"] = searchText;
    
    WeakSelf;
    
    [self.viewModel fetchClearWithBlock:^(NSArray *objects, NSError *error) {
        
        StrongSelf;
        
        if ([objects count]) {
            [s_self.tableView reloadData];
        }
        
    }];
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    CPBaseModel *model = self.viewModel.objects[indexPath.row];
    
    if (_action) {
        _action(model);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0;
}

@end
