//
//  CPBaseCell.h
//  akagi
//
//  Created by 王澍宇 on 15/10/8.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CPCommonTools.h"

@interface CPBaseCell : UITableViewCell

@property (nonatomic, weak) UITableView *tableView;

@property (nonatomic, strong) CPBaseModel *model;

@property (nonatomic, strong) UIView *line;

- (void)bindWithModel:(CPBaseModel *)model;

@end
