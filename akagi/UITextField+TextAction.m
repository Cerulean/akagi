//
//  UITextField+TextAction.m
//  akagi
//
//  Created by 王澍宇 on 15/11/3.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "UITextField+TextAction.h"

static NSString *_textActionKey  = @"textActionKey";
static NSString *_doneActionKey  = @"doneActionKey";
static NSString *_beginActionKey = @"beginActionKey";

@implementation UITextField (TextAction)

- (instancetype)init {
    if (self = [super init]) {
        [self addTarget:self action:@selector(textAction:)  forControlEvents:UIControlEventEditingChanged];
        [self addTarget:self action:@selector(doneAction:)  forControlEvents:UIControlEventEditingDidEndOnExit];
        [self addTarget:self action:@selector(beginAction:) forControlEvents:UIControlEventEditingDidBegin];
    }
    return self;
}

- (void)setTextAction:(CPTextAction)textAction {
    objc_setAssociatedObject(self, &_textActionKey, textAction, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CPTextAction)textAction {
    return objc_getAssociatedObject(self, &_textActionKey);
}

- (void)setDoneAction:(CPTextAction)doneAction {
    objc_setAssociatedObject(self, &_doneActionKey, doneAction, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CPTextAction)doneAction {
    return objc_getAssociatedObject(self, &_doneActionKey);
}

- (void)setBeginAction:(CPTextAction)beginAction {
    objc_setAssociatedObject(self, &_beginActionKey, beginAction, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CPTextAction)beginAction {
    return objc_getAssociatedObject(self, &_beginActionKey);
}

- (void)textAction:(id)sender {
    if (self.textAction) {
        self.textAction(self.text);
    }
}

- (void)doneAction:(id)sender {
    if (self.doneAction) {
        self.doneAction(self.text);
    }
}

- (void)beginAction:(id)sender {
    if (self.beginAction) {
        self.beginAction(self.text);
    }
}

@end
