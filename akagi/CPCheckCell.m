//
//  CPCheckCell.m
//  akagi
//
//  Created by 王澍宇 on 15/11/2.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPCheckCell.h"

@implementation CPCheckCell

- (void)bindWithModel:(CPBaseModel *)model {
    if ([model isKindOfClass:[CPBuilding class]]) {
        CPBuilding *building = (CPBuilding *)model;
        
        self.textLabel.text = building.name;
    }
}

@end
