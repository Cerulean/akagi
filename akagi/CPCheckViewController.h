//
//  CPCheckViewController.h
//  akagi
//
//  Created by 王澍宇 on 15/11/2.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import "CPBaseTableViewController.h"

typedef void(^CPCheckResultBlock)(NSArray *results);

@interface CPCheckViewController : CPBaseTableViewController

@property (nonatomic, strong) CPCheckResultBlock block;

@end
