//
//  UIWindow+Presentation.h
//  akagi
//
//  Created by 王澍宇 on 15/10/29.
//  Copyright © 2015年 Shuyu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIWindow (Presentation)

+ (void)presentViewController:(UIViewController *)viewController Animated:(BOOL)animated Completion:(void (void))completion;

@end
